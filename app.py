# Importing required packages
from flask import Flask, request, jsonify
import subprocess
import uuid
import os


PORT = os.environ.get('PORT') or 3000
# Initiating a Flask application
app = Flask(__name__)
dynamic_routes = {}

def execute_script(script):
    try:
        result = subprocess.run(script, shell=True, check=True, capture_output=True, text=True)
        return result.stdout
    except subprocess.CalledProcessError as e:
        return e.stderr
    
@app.route(rule='/create-route', methods=['POST'])
def create_route():
    data = request.get_json()
    schema = data.get('schema')
    script = data.get('script')
    if not schema or not script:
        return jsonify({"error": "Schema and script are required"}), 400

    route_id = str(uuid.uuid4())
    route_path = f'/execute/{route_id}'
    dynamic_routes[route_path] = {
        'script': script
    }

    return jsonify({"url": f'http://localhost:{PORT,route_path}'})

@app.route(rule='/execute/<route_id>', methods=['POST'])
def execute_route(route_id):
    route_path = f'/execute/{route_id}'
    if route_path not in dynamic_routes:
        return jsonify({"error": "Route not found"}), 404

    route_info = dynamic_routes[route_path]
    script = route_info['script']

    output = execute_script(script)
    headers = request.headers
    data = request.get_json()
    print(headers)
    print(data) 
    return jsonify({"output": output}), 200

# Running the API
if __name__ == "__main__":
    app.run(debug=True, port=PORT)