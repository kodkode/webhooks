import json
from typing import Any, Dict, List, Optional
from dataclasses import dataclass, field

def get_python_type(value: Any, name: str) -> str:
    if isinstance(value, bool):
        return "bool"
    elif isinstance(value, int):
        return "int"
    elif isinstance(value, float):
        return "float"
    elif isinstance(value, str):
        return "str"
    elif isinstance(value, list):
        if len(value) > 0:
            list_type = get_python_type(value[0], name)
            return f"List[{list_type}]"
        else:
            return "List[Any]"
    elif isinstance(value, dict):
        # Generate nested class
        nested_class_name = name.capitalize()
        nested_class = generate_data_class(nested_class_name, value)
        return nested_class_name
    else:
        return "Any"

def generate_data_class(name: str, json_obj: Dict[str, Any]) -> str:
    fields = []
    nested_classes = []
    for key, value in json_obj.items():
        python_type = get_python_type(value, key)
        fields.append(f"    {key}: {python_type}")
        if isinstance(value, dict):
            nested_class_name = key.capitalize()
            nested_class = generate_data_class(nested_class_name, value)
            nested_classes.append(nested_class)

    class_definition = f"@dataclass\nclass {name}:\n"
    class_definition += "\n".join(fields) + "\n"
    for nested_class in nested_classes:
        class_definition += "\n" + nested_class
    return class_definition

def create_schema_from_json(name: str, json_str: str) -> str:
    json_obj = json.loads(json_str)
    return generate_data_class(name, json_obj)
