import os
import re
import subprocess
import venv
import platform
import json

# Function to extract dependencies from a Python file

def get_dependencies(file_path):
    with open(file_path, 'r') as file:
        content = file.read()
        pattern = r'^dependencies\s*=\s*\{[^}]*\}'
        match = re.search(pattern, content, re.DOTALL)
        if match:
            data = match.group()
            dependencies_str = data.split("=")[1]
            dependencies = json.loads(dependencies_str)
            return dependencies
        if not match:
            dependencies = []
            for line in file:
                if line.startswith('import ') or line.startswith('from '):
                    line = re.sub(r'#.*$', '', line)
                    words = line.split()
                    if len(words) > 1:
                        dependencies.append(words[1])
            return dependencies

# Function to install Python dependencies
def install_dependencies(dependencies):
    venv_dir = 'venv'
    try:
        venv.create(venv_dir, with_pip=True)
        if platform.system() == 'Windows':
            activate_script = os.path.join(venv_dir, 'Scripts', 'activate.bat')
        else:
            activate_script = os.path.join(venv_dir, 'bin', 'activate')
        subprocess.run([activate_script], shell=True, check=True)
        if type(dependencies) == dict:
            for package, version in dependencies.items():
                print(f"Installing {package}=={version}")
                subprocess.run(['python', '-m', 'pip', 'install', f'{package}=={version}'], check=True)
        if type(dependencies) == list:
            for package in dependencies:
                print(f"Installing {package}")
                subprocess.run(['python', '-m', 'pip', 'install', package], check=True)
    except subprocess.CalledProcessError as e:
        print(f'Error installing dependencies: {e}')

def bundle(python_file):
    file = os.path.splitext(python_file)[0]
    subprocess.run(['pyinstaller', python_file, '--one-file'])
    subprocess.run(['pyinstaller', f'{file}.speck'])
    return os.path.join(os.getcwd(), f'dist/{file}')

# Main function
def get_bundled_file(python_file):
    python_dependencies = get_dependencies(python_file)
    install_dependencies(python_dependencies)
    return bundle(python_file)