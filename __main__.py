# Importing required packages
from flask import Flask, request, jsonify
import os

PORT = os.environ.get('PORT') or 3000
# Initiating a Flask application
app = Flask(__name__)
    
@app.route(rule='/api', methods=['POST'])
def create_route():
    data = request.get_json()
    headers = request.headers
    if not data:
        return jsonify({"error": "Schema and script are required"}), 400
    print(headers)
    print(data)
    return jsonify({"hello": 'world'}), 200

# Running the API
if __name__ == "__main__":
    app.run(debug=True, port=PORT)