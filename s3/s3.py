from flask import Flask, request, send_file
import boto3
import io
import uuid
import os
import tempfile
from ..bundle import bundle
S3_URL = os.environ.get('S3_URL') or 'http://localhost:4566'
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID') or 'test'
app = Flask(__name__)

# Configure S3 client
s3 = boto3.client('s3', endpoint_url=S3_URL,
                  aws_access_key_id='test', aws_secret_access_key='test')

# Create a bucket
bucket_name = 'my-local-bucket'
s3.create_bucket(Bucket=bucket_name)


# @app.route('/upload', methods=['POST'])
# def upload_file():
#     try:
#         file = request.files['file']
#         original_file_name = file.filename
#         file_extension = os.path.splitext(original_file_name)[1]  # Get the file extension
#         new_file_name = str(uuid.uuid4()) + file_extension  
#         s3.upload_fileobj(file, bucket_name, new_file_name)
#         return f'File {original_file_name} uploaded as {new_file_name} successfully!', 200
#     except Exception as e:
#         return str(e), 404
@app.route('/upload', methods=['POST'])
def upload_file():
    try:
        file = request.files['file']
        original_file_name = file.filename
        file_extension = os.path.splitext(original_file_name)[1]  # Get the file extension

        # Create a unique temporary directory
        temp_dir = tempfile.mkdtemp()
        # with tempfile.TemporaryDirectory() as temp_dir:
        uuid_name = str(uuid.uuid4())
        temp_file_path = os.path.join(temp_dir, uuid_name + file_extension)

        # Save the file temporarily
        file.save(temp_file_path)
        bundled_file = bundle.get_bundled_file(temp_file_path)
        # Upload the file to S3
        new_file_name = uuid_name + file_extension
        with open(temp_file_path, 'rb') as f:
            s3.upload_fileobj(f, bucket_name, new_file_name)

        return f'File {original_file_name} uploaded as {new_file_name} successfully!', 200
    except Exception as e:
        return str(e), 404

@app.route('/download/<file_name>', methods=['GET'])
def download_file(file_name):
    try:
        file_obj = s3.get_object(Bucket=bucket_name, Key=file_name)
        file_data = file_obj['Body'].read()
        return send_file(io.BytesIO(file_data), download_name=file_name, as_attachment=True), 200
    except Exception as e:
        return str(e), 404


@app.route('/edit/<file_name>', methods=['POST'])
def edit_file(file_name):
    try:
        file_obj = s3.get_object(Bucket=bucket_name, Key=file_name)
        file_data = file_obj['Body'].read()

        new_text = request.form.get('new_text')
        edited_file_data = new_text.encode('utf-8')

        s3.upload_fileobj(io.BytesIO(edited_file_data), bucket_name, file_name)
        return f'File {file_name} edited successfully!', 200
    except Exception as e:
        return str(e), 404


@app.route('/delete/<file_name>', methods=['DELETE'])
def delete_file(file_name):
    try:
        s3.delete_object(Bucket=bucket_name, Key=file_name)
        return f'File {file_name} deleted successfully!', 200
    except Exception as e:
        return str(e), 404

if __name__ == '__main__':
    app.run(debug=True)